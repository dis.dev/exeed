"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import tabs from './components/tabs.js'; // Tabs
import HystModal from 'hystmodal';
import Spoilers from "./components/spoilers.js";
import Dropdown from "./components/dropdown.js";

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()


// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')

// Вкладки (tabs)
tabs();

// Сворачиваемые блоки
collapse();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Dropdown
Dropdown();

// Select

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-select-item]')) {
        const select            = event.target.closest('[data-select]')
        const selectValue       =  select.querySelector('[data-select-value]')
        const selectPlaceholder = select.querySelector('[data-select-placeholder]')
        const selectItems       = select.querySelectorAll('[data-select-item]')
        const selectItem        = event.target.closest('[data-select-item]')
        const selectItemValue   = selectItem.dataset.selectItem

        selectItems.forEach(elem => {
            elem.classList.remove('selected');
        });
        selectItem.classList.add('selected')

        selectValue.value = selectItemValue
        selectPlaceholder.innerHTML = selectItemValue
        select.classList.remove('open')
    }
})


// Modal HystModal
const myModal = new HystModal({
    linkAttributeName: 'data-hystmodal',
});


// Sliders
import "./components/sliders.js";


