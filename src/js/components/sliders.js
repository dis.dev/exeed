/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/

import Swiper, {Autoplay, Navigation, Pagination, Thumbs} from 'swiper';

// Инициализация слайдеров
function initSliders() {
    // Перечень слайдеров
    if (document.querySelector('[data-swiper]')) {
        new Swiper('[data-swiper]', {
            // Подключаем модули слайдера
            // для конкретного случая
            //modules: [Navigation, Pagination],
            /*
            effect: 'fade',
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
            */
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 0,
            autoHeight: true,
            speed: 800,
            //touchRatio: 0,
            //simulateTouch: false,
            //loop: true,
            //preloadImages: false,
            //lazy: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            // Arrows
            navigation: {
                nextEl: '.swiper__more .swiper__more--next',
                prevEl: '.swiper__more .swiper__more--prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 16,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 24,
                },
                1024: {
                    slidesPerView: 4,
                    spaceBetween: 32,
                },
            },
            on: {}
        });
    }

    if (document.querySelector('[data-award-gallery]')) {
        new Swiper('[data-award-gallery]', {
            observer: true,
            observeParents: true,
            initialSlide: 1,
            slidesPerView: 1,
            spaceBetween: 12,
            autoHeight: true,
            speed: 800,
            breakpoints: {
                768: {
                    slidesPerView: 3,
                    spaceBetween: 12,
                },
                1320: {
                    slidesPerView: 3,
                    spaceBetween: 24,
                }
            }
        })
    }

    if (document.querySelector('[data-award]')) {
        new Swiper('[data-award]', {
            modules: [Pagination],
            observer: true,
            observeParents: true,
            initialSlide: 1,
            slidesPerView: 1,
            spaceBetween: 12,
            autoHeight: true,
            speed: 800,
            pagination: {
                el: "[data-award-pagination]",
                clickable: true,
            },
            breakpoints: {
                768: {
                    slidesPerView: 1,
                    spaceBetween: 12,
                },
                1320: {
                    slidesPerView: 2,
                    spaceBetween: 24,
                }
            }
        })
    }

    if (document.querySelector('[data-news]')) {
        new Swiper('[data-news]', {
            modules: [Pagination, Navigation],
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 30,
            autoHeight: true,
            speed: 800,
            pagination: {
                el: "[data-news-pagination]",
                clickable: true,
            },
            navigation: {
                nextEl: '[data-news-next]',
                prevEl: '[data-news-prev]',
            },
            breakpoints: {
                1320: {
                    slidesPerView: 2,
                    spaceBetween: 30,
                }
            }
        })
    }

    if (document.querySelector('[data-jury]')) {
        new Swiper('[data-jury]', {
            modules: [Pagination, Navigation],
            loop: true,
            observer: true,
            observeParents: true,
            slidesPerView: 1,
            spaceBetween: 30,
            autoHeight: true,
            speed: 800,
            pagination: {
                el: "[data-jury-pagination]",
                clickable: true,
            },
            navigation: {
                nextEl: '[data-jury-next]',
                prevEl: '[data-jury-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 1,
                    spaceBetween: 100,
                },
                1320: {
                    slidesPerView: 3,
                    spaceBetween: 80,
                },
                1570: {
                    slidesPerView: 3,
                    spaceBetween: 130,
                }
            }
        })
    }

    if (document.querySelector('[data-models]')) {
        new Swiper('[data-models]', {
            modules: [Pagination, Navigation],
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 16,
            autoHeight: true,
            speed: 800,
            pagination: {
                el: "[data-models-pagination]",
                clickable: true,
            },
            navigation: {
                nextEl: '[data-models-next]',
                prevEl: '[data-models-prev]',
            },
            breakpoints: {
                768: {
                    spaceBetween: 24,
                    slidesPerView: 'auto',
                },
                1320: {
                    spaceBetween: 24,
                    slidesPerView: 4,
                },
                1570: {
                    spaceBetween: 24,
                    slidesPerView: 4,
                }
            }
        })
    }

    if (document.querySelector('[data-press]')) {

        let pressQuerySize  = 768;
        let pressSlider = null;

        function pressSliderInit() {
            if(!pressSlider) {
                pressSlider = new Swiper('[data-press-content]', {
                    observer: true,
                    observeParents: true,
                    slidesPerView: 'auto',
                    spaceBetween: 16,
                    autoHeight: false,
                    speed: 800,
                    pagination: {
                        el: "[data-models-pagination]",
                        clickable: true,
                    },
                    navigation: {
                        nextEl: '[data-models-next]',
                        prevEl: '[data-models-prev]',
                    },
                    breakpoints: {
                        768: {
                            spaceBetween: 24,
                            slidesPerView: 'auto',
                        },
                        1320: {
                            spaceBetween: 24,
                            slidesPerView: 4,
                        },
                    }
                })
            }
        }

        function pressSliderDestroy() {
            if(pressSlider) {
                pressSlider.destroy();
                pressSlider = null;
            }
        }

        if (document.documentElement.clientWidth < pressQuerySize) {
            pressSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < pressQuerySize) {
                pressSliderInit()
            }
            else {
                pressSliderDestroy()
            }
        });

        new Swiper('[data-press-filter]', {
            modules: [Navigation],
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 0,
            autoHeight: true,
            speed: 800,
            navigation: {
                nextEl: '[data-press-nav]',
            },
        })
    }

    if (document.querySelector('[data-compilation]')) {

        new Swiper('[data-compilation]', {
            modules: [Pagination, Navigation],
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 16,
            autoHeight: true,
            speed: 800,
            navigation: {
                nextEl: '[data-compilation-next]',
                prevEl: '[data-compilation-prev]',
            },
            pagination: {
                el: "[data-compilation-pagination]",
                clickable: true,
            },
            breakpoints: {
                1320: {
                    spaceBetween: 28,
                    slidesPerView: 2,
                },
            }
        })
    }
}

window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});

