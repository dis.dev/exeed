export default () => {
    document.addEventListener('click',  (event) => {
        if(event.target.closest('[data-nav-toggle]')) {
            const body = document.querySelector('body')
            const nav = document.querySelector('[data-nav]')
            body.classList.toggle('nav-open');
            if (body.classList.contains('nav-open')) {
                nav.style.maxHeight = nav.scrollHeight + 'px';
            } else {
                nav.style.maxHeight = null;
            }
            return false;

        }
    })
};

